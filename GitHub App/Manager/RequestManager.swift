//
//  Networking.swift
//  GitHub App
//
//  Created by Matija on 31/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension UIViewController{
    
    //Gets data for github user
    func getUserData(url: String, completionHandler: @escaping (_ response: [GithubUser]) -> Void){
        AF.request(url).responseDecodable(of: GithubUser.self) { response in
            guard let data = response.data else{return}
            do{
                let myResponse = try JSONDecoder().decode(GithubUser.self, from: data)
                completionHandler([myResponse])
            }catch{print(error)}
        }
    }
    
    //Gets data for github repository
    func getRepoData(url: String, completionHandler: @escaping (_ response: [Item]) -> Void){
        AF.request(url).responseDecodable(of: GithubRepo.self) { response in
            guard let data = response.data else{return}
            do{
                
                let myResponse = try JSONDecoder().decode(GithubRepo.self, from: data)
                guard let response = myResponse.items else{return}
                completionHandler(response)
                self.dismissAlert()
            }catch{print(error)}
        }
    }
}
