//
//  Constants.swift
//  GitHub App
//
//  Created by Matija on 02/02/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import Foundation

let gitHubApiURL: String = "https://api.github.com"
let gitHubURL: String = "https://github.com/"
