//
//  ImageHelper.swift
//  GitHub App
//
//  Created by Matija on 31/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import Foundation
import UIKit

func setImage(from url: String, imageView: UIImageView) {
    guard let imageURL = URL(string: url) else { return }
    DispatchQueue.global().async {
        guard let imageData = try? Data(contentsOf: imageURL) else { return }
        
        let image = UIImage(data: imageData)
        DispatchQueue.main.async {
            imageView.image = image
        }
    }
}
