//
//  Helper.swift
//  GitHub App
//
//  Created by Matija on 29/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import UIKit
import Foundation

extension UIViewController {
    func showAlert(title: String?, message: String?, closeButtonTitle: String? = "Close") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: closeButtonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

func showToast(controller: UIViewController, message : String, seconds: Double){
    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    alert.view.backgroundColor = .black
    alert.view.alpha = 0.5
    alert.view.layer.cornerRadius = 15
    controller.present(alert, animated: true)
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
        alert.dismiss(animated: true)
    }
}
