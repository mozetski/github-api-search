//
//  LoadingHelper.swift
//  GitHub App
//
//  Created by Matija on 31/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension UIViewController{
    
    internal func dismissAlert() { if let vc = self.presentedViewController, vc is UIAlertController { self.dismiss(animated: false, completion: nil)
        }
    }
    
    func presentLoadingScreen(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
}

