//
//  Validator.swift
//  GitHub App
//
//  Created by Matija on 02/02/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import Foundation
import UIKit

func isSearchTermValid(searchTerm: String) -> Bool {
    return !(searchTerm.contains("/") || searchTerm.contains("&") || searchTerm.contains("=") || searchTerm.contains("?"))
    
}

func getFormattedSearchTerm(searchTerm: String) -> String{
    return searchTerm.replacingOccurrences(of: " ", with: "-", options: .literal, range: nil)
}

extension UIViewController {
    func isRequestValid(searchTerm: String) -> Bool {
        if !Reachability.isConnectedToNetwork() {
            showAlert(title: "No internet connection", message: "Please connected to the internet in order to use this service")
            return false
        }
        if searchTerm.isEmpty {
            showAlert(title: "Search field empty!", message: "Please enter a repository name you wish to search 🕵🏻")
            return false
        }
        if !isSearchTermValid(searchTerm: searchTerm) {
            showAlert(title: "Error", message: "Name must not contain special symbols")
            return false
        }
        return true
    }
    
    func isLoginValid(usernameText: String, passwordText: String) -> Bool{
        if !Reachability.isConnectedToNetwork() {
            showAlert(title: "No internet connection", message: "Please connected to the internet in order to use this service")
            return false
        }
        if usernameText.isEmpty || passwordText.isEmpty{
            showAlert(title: "Login failed!", message: "Please fill both fields in order to login")
            return false
        }
        return true
    }
    
    func checkResultValid(array: [Item], textfield: UITextField){
        if array.count == 0{
            textfield.placeholder = "No repository found"
            textfield.text = ""
        }else{
            textfield.placeholder = "Enter the repository name"
        }
    }
}
