//
//  Search.swift
//  GitHub App
//
//  Created by Matija on 30/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import UIKit
import Foundation

class SearchHandler: UIViewController{
    
    // Checks UserDefaults values and returns appropriate string
    private func getFilterUrl() -> String {
        if UserDefaults.standard.bool(forKey: "filterStars") == true {
            return "&sort=stars&order=desc"
        }else if UserDefaults.standard.bool(forKey: "filterForks") == true{
            return "&sort=forks&order=desc"
        }else if UserDefaults.standard.bool(forKey: "filterUpdated") == true{
            return "&sort=updated&order=desc"
        }else{
            return ""
        }
    }
    
    func getSearchUrl(githubUrl: String, searchTerm: String) -> String {
        return "\(githubUrl)/search/repositories?q=\(searchTerm)\(getFilterUrl())"
    }
    
    // Presents AlerDialog for selecting desired repository sorting option
    func presentFilterAlertDialog(controller: UIViewController, textField: UITextField){
        let alertController = UIAlertController(title: "Filter repositories", message: nil, preferredStyle: .alert)
        
        let actionOne = UIAlertAction(title: "Stars", style: .default) { (_) in  self.setFilterValues(0)}
        let actionTwo = UIAlertAction(title: "Forks", style: .default) { (_) in self.setFilterValues(1)}
        let actionThree = UIAlertAction(title: "Updated", style: .default) { (_) in self.setFilterValues(2)}
        let actionFour = UIAlertAction(title: "None", style: .default) { (_) in self.setFilterValues(3)}
        
        alertController.addAction(actionOne)
        alertController.addAction(actionTwo)
        alertController.addAction(actionThree)
        alertController.addAction(actionFour)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    // Sets UserDefaults values depending on the chosen option
    func setFilterValues(_ action: Int){
        switch action {
        case 0:
            UserDefaults.standard.set(true, forKey: "filterStars")
            UserDefaults.standard.set(false, forKey: "filterForks")
            UserDefaults.standard.set(false, forKey: "filterUpdated")
        case 1:
            UserDefaults.standard.set(false, forKey: "filterStars")
            UserDefaults.standard.set(true, forKey: "filterForks")
            UserDefaults.standard.set(false, forKey: "filterUpdated")
        case 2:
            UserDefaults.standard.set(false, forKey: "filterStars")
            UserDefaults.standard.set(false, forKey: "filterForks")
            UserDefaults.standard.set(true, forKey: "filterUpdated")
        case 3:
            UserDefaults.standard.set(false, forKey: "filterStars")
            UserDefaults.standard.set(false, forKey: "filterForks")
            UserDefaults.standard.set(false, forKey: "filterUpdated")
        default:
            break
        }
    }
}
