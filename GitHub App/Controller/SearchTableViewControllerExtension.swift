//
//  SearchTableViewControllerExtension.swift
//  GitHub App
//
//  Created by Matija on 25/02/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import Foundation

extension SearchTableViewController{
    func reloadTable(){
        if isRequestValid(searchTerm: UserDefaults.standard.string(forKey: "searchTerm") ?? "") {
            let searchUrl = search.getSearchUrl(githubUrl: gitHubApiURL, searchTerm: UserDefaults.standard.string(forKey: "searchTerm") ?? "")
            getRepoData(url: searchUrl, completionHandler: { (response) in
                self.repos = response
                self.checkResultValid(array: self.repos, textfield: self.repoTextField)
                self.tableView.reloadData()
            })
        }
    }
}
