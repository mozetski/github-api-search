//
//  SearchTableViewController.swift
//  GitHub App
//
//  Created by Matija on 27/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import UIKit
import Alamofire

class SearchTableViewController: UITableViewController {
    
    // MARK: - Outlets & Actions
    
    @IBAction func refreshCOntrolValueChanged(_ sender: UIRefreshControl) {
        if UserDefaults.standard.bool(forKey: "didSearch"){
            reloadTable()
        }
        sender.endRefreshing()
    }
    @IBOutlet weak var repoTextField: UITextField!
    @IBAction func searchRepoButtonPressed(_ sender: UIBarButtonItem) {
        guard let searchTerm = repoTextField.text else {return}
        if isRequestValid(searchTerm: searchTerm) {
            self.presentLoadingScreen()
            let formattedString = getFormattedSearchTerm(searchTerm: searchTerm)
            let searchUrl = search.getSearchUrl(githubUrl: gitHubApiURL, searchTerm: formattedString)
            UserDefaults.standard.set(formattedString, forKey: "searchTerm")
            getRepoData(url: searchUrl, completionHandler: { (response) in
                self.repos = response
                self.checkResultValid(array: self.repos, textfield: self.repoTextField)
                self.tableView.reloadData()
            })
        }
        UserDefaults.standard.set(true, forKey: "didSearch")
    }
    @IBAction func filterRepoButtonPressed(_ sender: UIBarButtonItem) {
        search.presentFilterAlertDialog(controller: self, textField: repoTextField)
    }
    @IBAction func userProfileButtonTapped(_ sender: UIBarButtonItem) {
        showAlert(title: "User info", message: "Username: \(UserDefaults.standard.string(forKey: "userLoginName") ?? "Unavailable")\n Biography: \(UserDefaults.standard.string(forKey: "userBiography") ?? "Unavailable")\n Email: \(UserDefaults.standard.string(forKey: "userEmail") ?? "Unavailable")")
    }
    
    // MARK: - Global variables
    
    var search = SearchHandler()
    var repos: [Item] = []
    var users: [GithubUser] = []
    var selectedElement: [Item] = []
    var loginObject = LoginViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(false, forKey: "didSearch")
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repos.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedElement = [repos[indexPath.item]]
        performSegue(withIdentifier: "repoDetailSegue", sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repoCell", for: indexPath) as! RepositoryDetailTableViewCell
        
        let element = repos[indexPath.row]
        cell.authorNameButton.setTitle(element.owner.login, for: .normal)
        cell.authorNameButton.tag = indexPath.item
        setImage(from: element.owner.avatarURL!, imageView: cell.authorImage)
        cell.repoNameLabel.text = "Repository name: \(element.name ?? "Unavailable")"
        cell.watchersNumberLabel.text = "Watchers: \(element.watchersCount ?? 0)"
        cell.forksNumberLabel.text = "Forks: \(element.forksCount ?? 0)"
        cell.issuesNumberLabel.text = "Issues: \(element.watchersCount ?? 0)"
        
        return cell
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "repoDetailSegue" {
            let detailVC = segue.destination as? RepositoryDetailViewController
            detailVC?.githubClientInDetailVC = selectedElement[0]
            detailVC?.repos = repos
        }
        if let button = sender as? UIButton {
            if segue.identifier == "userDetailSegue" {
                let detailVC = segue.destination as? UserDetailViewController
                detailVC?.authorName = repos[button.tag].owner.login!
            }
        }
    }
}
