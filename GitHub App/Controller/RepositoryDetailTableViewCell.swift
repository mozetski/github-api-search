//
//  RepositoryDetailTableViewCell.swift
//  GitHub App
//
//  Created by Matija on 28/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import UIKit

class RepositoryDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var authorImage: UIImageView!
    @IBOutlet weak var authorNameButton: UIButton!
    @IBOutlet weak var watchersNumberLabel: UILabel!
    @IBOutlet weak var forksNumberLabel: UILabel!
    @IBOutlet weak var issuesNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
