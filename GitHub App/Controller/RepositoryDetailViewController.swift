//
//  RepositoryDetailViewController.swift
//  GitHub App
//
//  Created by Matija on 28/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import UIKit

class RepositoryDetailViewController: UIViewController {
    
    // MARK: - Outlets & Actions
    
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var watcherNumberCount: UILabel!
    @IBOutlet weak var forkNumberLabel: UILabel!
    @IBOutlet weak var issuesNumberLabel: UILabel!
    @IBOutlet weak var programLanguageLabel: UILabel!
    @IBOutlet weak var dateOfCreationLabel: UILabel!
    @IBOutlet weak var dateModifiedLabel: UILabel!
    @IBOutlet weak var ownerNameButton: UIButton!
    @IBAction func openRepoButton(_ sender: UIButton) {
        guard let url = URL(string: githubClientInDetailVC?.htmlURL ?? "https://github.com/") else { return }
        UIApplication.shared.open(url)
    }
    
    // MARK: - Global variables
    
    var githubClientInDetailVC: Item?
    var repos: [Item] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateRepoData()
    }
    
    func updateRepoData(){
        guard let selected = githubClientInDetailVC else{return}
        repoNameLabel.text = "Repository name: \(selected.name ?? "Unavailable")"
        watcherNumberCount.text = "Watchers number: \(selected.watchersCount ?? 0)"
        forkNumberLabel.text = "Watchers number: \(selected.forksCount ?? 0)"
        issuesNumberLabel.text = "Watchers number: \(selected.openIssuesCount ?? 0)"
        programLanguageLabel.text = "Program language: \(selected.language ?? "Unavailable")"
        dateOfCreationLabel.text = "Date of creation: \(selected.createdAt ?? "Unavailable")"
        dateModifiedLabel.text = "Date modified: \(selected.updatedAt ?? "Unavailable")"
        ownerNameButton.setTitle(selected.owner.login ?? "Unavailable", for: .normal)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "userDetailSegues" {
            let detailVC = segue.destination as? UserDetailViewController
            guard let selected = githubClientInDetailVC else{return}
            detailVC?.authorName = selected.owner.login!
        }
    }
}
