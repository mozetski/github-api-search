//
//  UserDetailViewController.swift
//  GitHub App
//
//  Created by Matija on 28/01/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import UIKit
import Alamofire

class UserDetailViewController: UIViewController {
    
    // MARK: - Outlets & Actions
    
    @IBOutlet weak var authorImage: UIImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var authorEmailLabel: UILabel!
    @IBOutlet weak var authorBiographyLabel: UILabel!
    @IBOutlet weak var authorPublicReposLabel: UILabel!
    
    @IBAction func userDetailsButtonTapped(_ sender: UIButton) {
        guard let selected = selectedUser else{return}
        guard let url = URL(string: selected[0].htmlURL ?? gitHubURL) else { return }
        UIApplication.shared.open(url)
    }
    
    // MARK: - Global variables
    
    var selectedUser: [GithubUser]?
    var authorName: String = "Unknown"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserData(url: "\(gitHubApiURL)/users/\(authorName)", completionHandler: {(response) in
            self.selectedUser = response
            self.updateUserData()
        })
    }
    
    func updateUserData(){
        guard let selected = selectedUser else{return}
        setImage(from: selected[0].avatarURL, imageView: authorImage)
        authorNameLabel.text = "Author name: \(selected[0].login ?? "Unavailable")"
        authorEmailLabel.text = "Email: \(selected[0].email ?? "Unavailable")"
        authorBiographyLabel.text = "Biography: \(selected[0].bio ?? "Unavailable")"
        authorPublicReposLabel.text = "Public repos: \(selected[0].publicRepos ?? 0)"
    }
}
