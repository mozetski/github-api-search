//
//  LoginViewController.swift
//  GitHub App
//
//  Created by Matija on 01/02/2020.
//  Copyright © 2020 Matija Ožetski. All rights reserved.
//

import UIKit
import GithubAPI

class LoginViewController: UIViewController {
    
    // MARK: - Outlets & Actions
    
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        guard let username = usernameTextField.text, let password = passwordTextField.text else{return}
        if isLoginValid(usernameText: username, passwordText: password){
            let authentication = BasicAuthentication(username: username, password: password)
            UserAPI(authentication: authentication).getUser { (response, error) in
                if let _response = response {
                    if _response.login == nil{
                        DispatchQueue.main.async {
                            self.showAlert(title: "Login failed!", message: "Username/password incorrect or account does not exist")
                        }
                    }else{
                        DispatchQueue.main.async {
                            UserDefaults.standard.set(_response.login, forKey: "userLoginName")
                            UserDefaults.standard.set(_response.bio, forKey: "userBiography")
                            UserDefaults.standard.set(_response.email, forKey: "userEmail")
                            let vc = self.storyboard!.instantiateViewController(withIdentifier: "searchNavigationController") as! UINavigationController
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.showAlert(title: "Error!", message: "Unknown error occured while trying to login. Please try again.")
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
